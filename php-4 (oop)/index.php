<?php
include_once('animal.php');
include_once('frog.php');
include_once('ape.php');

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br/>";
echo $sheep->legs; // 2
echo "<br/>";
echo var_dump($sheep->cold_blooded); // false;
echo "<br/>";
echo "<br/>";
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br/>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "<br/>";