<?php
function ubah_huruf($string){
    $offset = 1 % 26;
    $newText = "";
    if($offset < 0) {
        $offset += 26;
    }
    $i = 0;
    while($i < strlen($string)) {
        $c = strtolower($string{$i}); 
        $newText .= chr(ord($c) + $offset);
      $i++;
    }
    return $newText. "<br/>";    
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>